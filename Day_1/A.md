# Modul 114
## A: DATEN CODIEREN 1: Zahlensysteme, Bit&Bytes

### Aufgabe 1

| Dezimalwerte | Hexadezimalwerte | Binärwerte | Binärwerte | Binärwerte | Binärwerte |
-------- | -------- | -------- | -------- | -------- | -------- |
0  | 00   | 0 | 0 | 0 | 0 |
1  | 01   | 0 | 0 | 0 | 1 |
2  | 02   | 0 | 0 | 1 | 0 |
3  | 03   | 0 | 0 | 1 | 1 |
4  | 04   | 0 | 1 | 0 | 0 |
5  | 05   | 0 | 1 | 0 | 1 |
6  | 06   | 0 | 1 | 1 | 0 |
7  | 07   | 0 | 1 | 1 | 1 |
8  | 08   | 1 | 0 | 0 | 0 |
9  | 09   | 1 | 0 | 0 | 1 |
10  | 0A   | 1 | 0 | 1 | 0 |
11  | 0B   | 1 | 0 | 1 | 1 |
12  | 0C   | 1 | 1 | 0 | 0 |
13  | 0D   | 1 | 1 | 0 | 1 |
14  | 0E   | 1 | 1 | 1 | 0 |
15  | 0F   | 1 | 1 | 1 | 1 |

Die Zahlensysteme folgen offensichtlich einem Muster. z.B. Binärwerte: 0001 -> 0010 (Eins wandert von Rechts nach Links)

### Aufgabe 2

Dezimalwert: 911
Binärwert: 11 1000 1111

### Aufgabe 3

Dezimalwert: 6+16+32+128 = 182
Binärwert: 1011'0110

### Aufgabe 4

Binärwert:  1110'0010'1010'0101
Hexadezimalwert: E2A5

### Aufgabe 5

1101'1001 + 0111'0101
9+16+64+128 + 1+4+16+32+64 = 334
1'0100'1110 => Pufferüberlauf

### Aufgabe 6

a. 1100 0000 . 1010 1000 . 0100 1100 . 1101 0011
Dec: 192.168.76.211

Hierbei handelt es sich offensichtlich um eine IPv4-Adresse, welche durch die IANA für die Nutzung innerhalb von LANs freigegeben ist.

b. 1011 1110 - 1000 0011 - 1000 0101 - 1101 0101 - 1110 0100 - 1111 1110
Hex: BE 83 85 D5 E4 FE

### Aufgabe 7

''chmod 751 CreateWeeklyReport'' => mittels chmod kann unter GNU/Linux Distributionen die Berechtigung einer Datei bzw. eines Verzeichnisses modifiziert/geändert werden. 751 definiert die Berechtigungen für Eigentümer, Eigentümer-Gruppe und Andere mittels oktaler Werte:

4 - lesen

2 - schreiben

1 - ausführen

0 - keine Rechte

### Aufgabe 8

2^1 => 2 Möglichkeiten
2^2 => 4 ...
2^3 => 8
2^4 => 16
2^5 => 32
2^6 => 64
2^7 => 128

7 Bits


### Aufgabe 9

Speicherkapazität in KiB = Adressbusbreite:8 ​× Datenbusbreite

Speicherkapazität in KiB=12:8​×16

Speicherkapazität in KiB=3:2×16 

Speicherkapazität in KiB=24

Die Speicherkapazität beträgt also 24 KiB.

### Aufgabe 10
a. Wenn die Verbindung seriell ist und ein Taktsignal von 1 MHz verwendet wird, können Sie die maximale Übertragungsrate in Bytes pro Sekunde wie folgt berechnen:

Übertragungsrate = Taktfrequenz x Datenbreite

In diesem Fall beträgt die Datenbreite 1 Bit, da die Verbindung seriell ist. 

Übertragungsrate = 1 MHz x 1 Bit = 1 MBit/s
Um die Übertragungsrate in Bytes pro Sekunde zu erhalten, müssen wir durch 8 teilen (da 1 Byte = 8 Bit):
Übertragungsrate in Bytes/s = Übertragungsrate in Bits/s : 8

Übertragungsrate in Bytes/s = 1 MBit/s : 8 = 125 kB/s


b. Wenn die Verbindung nicht seriell, sondern 8 Bit-parallel wäre, wäre die Datenbreite 8 Bit. Die Übertragungsrate würde dann wie folgt berechnet:

Übertragungsrate = Taktfrequenz x Datenbreite
Übertragungsrate = 1 MHz x 8 Bit = 8 MBit/s
Um die Übertragungsrate in Bytes pro Sekunde zu erhalten, teilen Sie wieder durch 8:

Übertragungsrate in Bytes/s = 8 MBit/s : 8 = 1 MB/s

Also, wenn die Verbindung 8 Bit-parallel ist, könnten 1 MB pro Sekunde übertragen werden.


### Reflexion

- Unterrichtsinhalte und Ziele:
Repetition bereits bekannter Themen (Zahlensysteme, Aussagenlogik sowie Umrechungen von Grösseneinheiten)

- Unterrichtsresultate:
Keine grossen Wissensgewinne -> Bestehendes Wissen/Themen wurden repetiert, insbesondere Zahlensysteme um Umrechnung in und aus diesen war bereits zur Genüge bekannt und vertraut.

- Probleme/Knacknüsse:
Aufgabe 10 war eher komplex zu lösen, Recherche nach Formeln und Rechnungen war erforderlich

- Neue Applikationen, Werkzeuge, Kommandos etc.:
Notepad++ wurde in den Unterlagen erwähnt und darauf verwiesen -> Entsprechende Applikation installiert und Funktionalitäten ausgetestet.

- Offene Fragen:
Es bestehen zur Zeit keine offenen Fragen

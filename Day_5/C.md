# Kapitel-C: BILDER CODIEREN UND KOMPRIMIEREN
## Valentin Binotto

1.	Suchen sie im Internet ein Bild vom Matterhorn, das eine Grösse von mind. 3000 Pixel aufweist und laden sie es auf ihren Notebook herunter. Danach bearbeiten sie es in einer Bildbearbeitungs-Software ihrer Wahl.
a.	Da das Bild eine viel zu hohe Auslösung hat, rechnen sie es herunter. Bei dieser Gelegenheit ändern sie das Bildseitenverhältnis auf 16:9. Sie werden sich für einen Bildausschnitt entscheiden müssen. Das Bild soll schlussendlich 720 Bildzeilen ausweisen.
b.	Speichern sie das Bild als JPG in höchster und tiefster Qualität ab, zudem auch als PNG ohne Transparenz. Notieren sie sich die erforderlichen Speichergrössen. Im Anschluss berechnen sie den unkomprimierten, theoretischen Speicherbedarf bei 8 Bit pro Farbkanal in MiB. Vergleichen sie die Werte und erklären sie die Unterschiede.
(Empfohlen wird das Online-Bildbearbeitungswerkzeug auf www.pixlr.com.)

- Niedrigste Qualität JPG: 129kb
    - 129'000 b -> 8*3 = 24 -> 129'000:24= 5375 Bit
- Höchste Qualität JPG: 6.7mb
    - 6'700'000 b -> 8*3 = 24 -> 6'700'000:24 = 279166.666667 Bit
- PNG ohne Transparenz: 11.6mb
    - 11'600'000 b -> 8*3 = 24 -> 11'600'000:24 = 483333.333333 Bit

2.	Nun erstellen sie aus demselben Matterhorn-Bild ein rundes Matterhorn-Logo:
a.	Schneiden sie das Matterhorn kreisförmig aus, damit eine Matterhorn-Medaille entsteht. Der Durchmesser soll 640 Pixel betragen. Beschriften sie das runde Bildchen mit dem Text "Matterhorn" und speichern sie es als PNG mit Transparenz ab.

- 659 KB

b.	Suchen sie im Internet ein geeignetes Farbmuster, das als Hintergrundbild dienen soll. Laden sie das gewählte Bild auf ihren Notebook herunter und bearbeiten sie es in ihrer Grafikapplikation wie folgt: Reduzieren sie die Farbsättigung derart, dass die Farben nur noch schwach angedeutet werden. Reduzieren sie auch den Kontrast. Speichern sie das Hintergrundbild in der Grösse 2000 Pixel x 2000 Pixel ab.

c.	Überprüfen sie ihr Werk mit diesem HTML-Code als Webseite:
<html>
  <head>
    <style>
      body {
        background-image: url('myBackground.jpg');
        background-repeat: no-repeat; }
    </style>
  </head>
  <body>
  <img src="Matterhorn.png">
  </body>
</html>

3.	Berechnen sie den Speicherbedarf für ein unkomprimiertes Einzelbild im HD720p50-Format bei einer True-Color-Farbauflösung.

Speicherbedarf = Breite × Höhe × Bit pro Pixel
Speicherbedarf in KB = 1280×720×24​ / 8 × 1024 = 2700KB

4.	Welchen Speicherbedarf hat das Video aus der vorangegangenen Aufgabe bei einer Spieldauer von 3 Minuten?

Da es sich um ein Video im HD720p50-Format handelt, beträgt die Bildrate (Bilder pro Sekunde) 50. Die Spieldauer beträgt 3 Minuten = 180 Sekunden.

Speicherbedarf für das Video = 2700 KB/Bild × 50 Bilder/Sekunde × 180 Sekunden
24'300'000KB =  23'691,41 MB = ca. 23.7 GB

5.	Ihre Digitalkamera bietet für die Speicherung ihrer Bilder folgende Formate an: RAW, TIF, JPG. Erklären sie in ein paar kurzen Sätzen die Unterschiede und Einsatzgebiete dieser drei Formatvarianten.

**RAW-Format:**
   - **Eigenschaften:** Das RAW-Format ist ein unkomprimiertes Bildformat, das die Rohdaten direkt von der Kamera ohne Verluste speichert. Es enthält alle Informationen, die von der Kamera-Sensor erfasst wurden.
   - **Vorteile:** Hohe Bildqualität, da keine Kompression oder Verluste auftreten. Es bietet mehr Flexibilität bei der nachträglichen Bildbearbeitung, insbesondere in Bezug auf Belichtung und Farbkorrektur.
   - **Einsatzgebiete:** Profis und Fotografen, die umfassende Bearbeitungsmöglichkeiten wünschen, nutzen RAW-Dateien. Dieses Format ist ideal für professionelle Fotografie und kreative Bildbearbeitung.

**TIF-Format (Tagged Image File):**
   - **Eigenschaften:** Das TIF-Format ist ein verlustfreies Bildformat, das eine hohe Qualität beibehält. Es unterstützt auch Mehrschicht-Bilder und verschiedene Farbräume.
   - **Vorteile:** Gute Bildqualität ohne verlustbehaftete Kompression. Es ist vielseitig einsetzbar und unterstützt professionelle Anwendungen, darunter Druck- und Grafikdesign.
   - **Einsatzgebiete:** TIF wird oft in der professionellen Druck- und Verlagsbranche verwendet, wo höchste Bildqualität und Flexibilität gefragt sind.

**JPG-Format (Joint Photographic Experts Group):**
   - **Eigenschaften:** Das JPG-Format ist ein komprimiertes Bildformat, das Verluste aufweisen kann. Es verwendet Verlustkompression, um die Dateigröße zu reduzieren, was zu einem gewissen Qualitätsverlust führen kann.   
   - **Vorteile:** Platzsparend und weit verbreitet. Gut geeignet für den alltäglichen Gebrauch und Webanwendungen, da es kleinere Dateigrößen ermöglicht.
   - **Einsatzgebiete:** JPG wird häufig für den Einsatz im Internet, sozialen Medien und anderen Anwendungen verwendet, bei denen Dateigröße eine Rolle spielt und geringe Qualitätsverluste akzeptabel sind.


6.	Sie möchten ihr neulich erstelltes Gameplay-Video auf Youtube veröffentlichen. Was sind die technischen Vorgaben dazu? (Format, Bildrate, Farbauflösung, Video-, Audiocodec etc.). Gibt es allenfalls rechtliche Einschränkungen?

YouTube hat spezifische technische Anforderungen für die Veröffentlichung von Videos. Hier sind einige grundlegende technische Vorgaben:

**Videoformat und -codec:**
   - **Format:** YouTube akzeptiert verschiedene Formate, aber MP4 ist am weitesten verbreitet.
   - **Codec:** H.264 ist der empfohlene Videocodec für YouTube. Alternativ kann auch H.265 (HEVC) verwendet werden.

**Bildrate (Frame Rate):**
   - Die empfohlene Bildrate beträgt 24, 25, 30, 48, 50 oder 60 Bilder pro Sekunde (fps).

**Farbauflösung (Resolution):**
   - YouTube unterstützt verschiedene Auflösungen. Für Gameplay-Videos häufig min. 1080p (1920x1080) oder sogar 4K (3840x2160) empfohlen.

**Bitrate:**
   - Die empfohlene Bitrate variiert je nach Auflösung und Bildrate. Höhere Auflösungen erfordern in der Regel höhere Bitraten für eine gute Qualität.

**Audiocodec:**
   - AAC (Advanced Audio Codec) mit einer Abtastrate von 44.1 kHz oder 48 kHz.

**Dateigröße:**
   - Die maximale Dateigröße beträgt 128 GB.

**Rechtliche Einschränkungen:**
   - Es muss sichergestellt werden, dass keine Urheberrechte verletzt werden (z.B. Musik im Hintergrund, Bilder, u.U. auch Inhalte von Games etc.)

7.	Sie haben ein 30-Zoll-Display (Diagonale) im Format 16:10 und 100ppi erworben. Was ist die Pixelauflösung horizontal und vertikal?

Anzahl der Pixel auf der Bildschirmdiagonalen = Wurzel aus: (Pixel horizontal)² + (Pixel vertikal)² 
Pixelanzahl = Bildschrimdiagonale x PPI / w(Seitenverhältnis^2 + 1)

2560 x 1600 px

8.	Sie drucken ein quadratisches Foto mit einer Kantenlänge von 2000 Pixel mit 600dpi. Wie gross in cm wird dieses?

Grösse in Zentimetern = (DPI : Pixelanzahl) ​× (2.54:100​)

9.	Ein kleiner Abstecher in die Welt der Töne:
Bild-1 zeigt den Aufbau eines Mikrofons, Bild-2 den eines Lautsprechers. Was sind eigentlich vom technischen Prinzip her die Unterschiede? Sowohl Lautsprecher wie Mikrofon arbeiten analog. Da der Computer nur "Digital" versteht, muss das analoge Signal zunächst in ein digitales Signal umgewandelt werden. Dafür sorgt ein sogenannter A/D-Wandler. Können sie seine Funktion in groben Zügen erklären? Bild-3 kann ihnen dabei weiter helfen.

Ein Analog-Digital-Wandler (A/D-Wandler) wandelt kontinuierliche analoge Signale in diskrete digitale Signale um. Hier sind die grundlegenden Schritte seiner Funktionsweise:

**Abtastung (Sampling):**
   - Das analoge Signal wird in regelmäßigen Abständen gemessen, um sogenannte Samples zu erhalten.

**Quantisierung:**
   - Die gemessenen Samples werden auf den nächstgelegenen digitalen Wert abgerundet.
   - Dieser Schritt bestimmt die Auflösung des digitalen Signals.

**Codierung:**
   - Die quantisierten Werte werden in binäre Codes umgewandelt.
   - Diese Binärcodes repräsentieren das digitale Signal.


Folgende Opensource-Programme können dabei nützlich sein:
•	https://www.gimp.org/
•	https://pixlr.com/de/
•	https://inkscape.org/de/
- http://www.retracked.net/pixeldichte/

### Aufgabe 1
 
Aufgabenstellung:
 
Warum bentötigt man AD-Wandler?
 
Lösung:
 
AD-Wandler ermöglichen die Integration von analogen Signalen in digitale Systeme, was in der heutigen Welt der Elektronik und Informationsverarbeitung von entscheidender Bedeutung ist.
 
 
### Aufgabe 2
 
Aufgabenstellung:
 
Warum geht eine A/D-Wandlung immer mit einem Datenverlust einher?
 
Lösung:
 
Beim AD Wandeln geht mit einem potenziellen Datenverlust einher, weil sie eine kontinuierliche Bandbreite von analogen Werten auf eine begrenzte Anzahl von diskreten digitalen Werten abbilden muss. Dieser Prozess ist mit bestimmten Einschränkungen verbunden, die zu einer gewissen Ungenauigkeit führen können.
 
 
### Aufgabe 3
 
Aufgabenstellung:
 
Gibt eine höhere oder eine tiefer Samplingrate eine präzisere Abbildung des Originals? Begründen sie!
 
Lösung:
 
Ich denke, ist es ideal, die Samplingrate so zu wählen, dass sie ausreichend ist, um alle relevanten Informationen des analogen Signals zu erfassen, ohne unnötig hohe Anforderungen an die Speicher- und Rechenressourcen zu stellen. Die Nyquist-Shannon-Abtasttheorem bietet eine Richtlinie dafür – die Samplingrate sollte mindestens doppelt so hoch sein wie die höchste Frequenzkomponente im Signal. Eine zu niedrige Samplingrate kann zu Aliasing-Verzerrungen und Informationsverlust führen, während eine unnötig hohe Samplingrate Ressourcen verbraucht, ohne zusätzlichen Nutzen zu bringen.
 
 
### Aufgabe 4
 
Aufgabenstellung:
 
Kann man durch die Bildumwandlung vom RGB- in den YCbCr-Farbraum Speicherplatz einsparen?
 
Lösung:
 
Nein, da auch hier noch mit drei Kanälen gearbeitet wird 
 
### Aufgabe 5
 
Aufgabenstellung:
 
Kann ein Beamer ein Bikld im YCbCr-Farbbereich darstellen?
 
Lösung:
 
Ja, viele Beamer sind in der Lage, Bilder im YCbCr-Farbbereich darzustellen. Der YCbCr-Farbraum ist ein allgemeiner Farbraum, der in der Videotechnik häufig verwendet wird.
 
 
### Aufgabe 6
 
Aufgabenstellung:
 
Wie rechnet man ein Farbbildes in ein Graustufenbild um?
 
Lösung:
 
Man kann gantz einfach der Durchschnitt von Grün, Blau und Rot berechen:
 
Rot + Grün + Blau
------------------   = Graustufe
        3
 
 
### Aufgabe 7
 
Aufgabenstellung:
 
Warum hat bei der Umwandlung eine Farbbildes in ein Graustufenbild der Grünanteil am meisten Gewicht?
 
Lösung:
 
Das ganze ist vom Mensch abhängig. Es gibt verschiedene Faktoren, die hier eine Rolle spielen. Darunter die Helligkeitswahrnehmung, Bildaufnahme und evt. auch Sehbehinderungen (Farbenblindheit)
 
 
### Aufgabe 8
 
Aufgabenstellung:
 
Warum verschlechter sich die Bildschärfe von 4:1:1-Subsampling gegenüber 4:4:4-Subsampling nicht?
 
Lösung:
 
 
Die Bildschärfe verschlechtert sich bei 4:1:1-Subsampling im Vergleich zu 4:4:4-Subsampling, da 4:1:1 eine Form des Chrominanz-Subsamplings ist, bei der die Farbinformationen in horizontaler Richtung deutlich reduziert werden. Die Zahlen in der Notation 4:1:1 repräsentieren das Verhältnis der Abtastung für die Lumen zur Chromen.
 
 
### AUfgabe 9
 
Aufgabenstellung:
 
Ein quadratisches 24-Bit-RGB-Bild mit einer Kantenlänge von 1000 Pixel soll mit 4:1:1 unterabgetatstet werden.
Wieviel Speicherplatz wird damit eingespart?
 
Lösung:
 
Wenn man die eingesparten MB eines Bildes berechnen möchte, dann rechnet man zuerst alle Pixel aus, in diesem Fall sind das 1000 pro Kante
heisst: 1000 x 1000 = 1'000'000
 
Von den Bits her sind es 24 Bits pro Pixel = 24'000'000
 
man rechnet also:
 
1'000'000 x 24 Bit/Pixel = 24'000'000 Bits = 3'000'000 Bytes = 2'929'... kB = etwa 2'900 MB eingespart
 
 
### Aufgabe 10
 
Aufgabenstellung:
 
Was ist der erste Schritt bei der JPG-Komprimierung?
 
Lösung:
 
Farbumwandlung, Unterabtastung, Transformation in den Frequenzbereich, Quantisierung, Huffman Codierung
 
 
### Aufgabe 11
 
Aufgabenstellung:
 
Führt die DCT-Transformation zu einer Datenreduktion?
 
Lösung:
 
Die DCT in der JPEG-Komprimierung ist wie ein besonderer Übersetzer für Bilder. Er packt die wichtigen Teile ins Kernige, während die weniger wichtigen Details in den Ecken landen. Das spart Platz und macht die Bilddatei kleiner, ohne dabei die wichtigen Dinge zu vergessen.
 
 
### Aufgabe 12
 
Aufgabenstellung:
 
Warum erhält man bei einer sehr starken Bildkomprimierung sogenante Block-Artefakte?
 
Lösung:
 
Bei sehr starken Bildkomprimierungen treten sogenannte Blockartefakte aufgrund der spezifischen Art und Weise auf, wie Bilder bei der JPEG-Komprimierung in Blöcke unterteilt und verarbeitet werden.
 
 
### Aufgabe 13
 
Aufgabenstellung:
 
Was ist der Unterschied zwischen Intraframe- und Interframe-Komprimierung
 
Lösung:
 
Intraframe- und Interframe-Komprimierung sind zwei unterschiedliche Ansätze in der Videokomprimierung, die auf verschiedene Weisen mit den einzelnen Frames (Bildern) in einem Videostream umgehen.
 
 
### Aufgabe 14
 
Aufgabenstellung:
 
Bei welcher Filmsequenz bietet die Interframekomprimierung mehr Potential zur Datenreduzierung?
a. 30 Sekunden Faultier auf Nahrungssuche
b. 30 Sekunden Zieleinfahrt beim Formel-1-Rennen
 
Lösung:
 
Option a, da das Faultier auf Nahrungssuche ist, bewegt es sich wahrscheinlich langsam, und die Szene kann relativ statisch sein. In solchen Szenen gibt es tendenziell weniger Veränderungen zwischen aufeinanderfolgenden Frames, was das Potenzial für Datenreduzierung durch Interframekomprimierung erhöht.
 
 
### Aufgabe 15
 
Aufgabenstellung:
 
Sehen sie Parallelen zwischen Datenbackupkonzepten und Interframe-Komprimierung?
 
Lösung:
 
Ja, es gibt Parallelen zwischen Datenbackupkonzepten und Interframe-Komprimierung, insbesondere wenn man bestimmte Aspekte in Betracht zieht: Inkramentelle Sicherungen, Nutzung von Ressourcen, Redundanzreduktion
 
 
### Aufgabe 16
 
Aufgabenstellung:
 
Was versteht man unter GOP25?
 
Lösung:
 
"GOP25" steht für "Group of Pictures 25" und bezieht sich auf die Gruppenstruktur in einem Videokompressionsverfahren. In der Videokompression werden Bilder oft in Gruppen organisiert, und "GOP" ist eine Abkürzung für "Group of Pictures" oder "Gruppe von Bildern". Die Zahl "25" in "GOP25" gibt an, wie viele Bilder in dieser Gruppe enthalten sind.

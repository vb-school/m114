# Kapitel-C: BILDER CODIEREN UND KOMPRIMIEREN
## Valentin Binotto


Aufgabe: Bestimmen sie die Farben für die folgenden RGB-Farbcodes (in HEX). Nutzen sie den RGB-Farbenmixer:
```
•	#FF0000 entspricht der Farbe Rot
•	#00FF00 entspricht der Farbe Grün
•	#0000FF entspricht der Farbe Blau
•	#FFFF00 entspricht der Farbe Rot + Grün = Gelb
•	#00FFFF entspricht der Farbe Grün + Blau = Cyan
•	#FF00FF entspricht der Farbe Rot + Blau = Magenta
•	#000000 entspricht der Farbe Weiss
•	#FFFFFF entspricht der Farbe Schwarz
•	#00BC00 entspricht der Farbe ....
```


Aufgabe: Bestimmen sie die Farben für die folgenden prozentualen CMYK-Angaben. Nutzen sie den CMYK-Farbenmixer:
```
•	C:0%, M:100%, Y:100%, K:0% entspricht der Farbe Magenta + Gelb = Rot
•	C:100%, M:0%, Y:100%, K:0% entspricht der Farbe Cyan + Gelb = Grün
•	C:100%, M:100%, Y:0%, K:0% entspricht der Farbe Cyan + Magenta = Blau
•	C:0%, M:0%, Y:100%, K:0% entspricht der Farbe Gelb
•	C:100%, M:0%, Y:0%, K:0% entspricht der Farbe Cyan
•	C:0%, M:100%, Y:0%, K:0% entspricht der Farbe Magenta
•	C:100%, M:100%, Y:100%, K:0% entspricht der Farbe Schwarz
•	C:0%, M:0%, Y:0%, K:100% entspricht der Farbe Keyfarbe
•	C:0%, M:0%, Y:0%, K:0% entspricht der Farbe Weiss
•	C:0%, M:46%, Y:38%, K:22% entspricht der Farbe ....
```




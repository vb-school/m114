# Modul 114
## A: DATEN CODIEREN 1: Zahlensysteme, Bit & Bytes

### Aufgabe 11
Nun zur Aufgabe: Wir gehen von einer Verarbeitungsbreite von einem Byte aus.
(Datenbusbreite:1Byte)

a. Nennen sie kleinster und grösster Binärwert bzw. Dezimaläquivalent im Falle
von unsigned bzw. Vorzeichenlos.

00000000 => 0
11111111 => 255

b. Nennen sie kleinster und grösster Binärwert bzw. Dezimaläquivalent im Falle
von signed bzw. Vorzeichenbehaftet.

10000000 => -128
01111111 => 127

c. Wandeln sie die Dezimalzahl +83 in einen vorzeichenbehafteten Binärwert
um. (signed)

01010011 => +83

d. Wandeln sie die Dezimalzahl -83 in einen vorzeichenbehafteten Binärwert um.
(signed)

10101101 => -83

e. Addieren sie die beiden erhaltenen Binärwerte zusammen. Es sollte 0
ergeben!

(+01010011) + 
1010 1101 -> 01010011 <br>
0000 0000

f. Wandeln sie die Dezimalzahl 0 in einen vorzeichenbehafteten Binärwert um.
(signed). Hat ihre vorangegangene Addition auch diesen Binärwert ergeben?

0000 0000

g. Warum können sie bei der gegebenen Datenbusbreite von 1 Byte die
Dezimalzahl +150 nicht in einen vorzeichenbehafteten Binärwert umwandeln?
(Ziehen sie daraus ihre Lehren für zukünftige Programmiersprachkurse: Immer den korrekten
Datentyp in der verlangten Grösse wählen)

1 Byte = 8 Bit => 2^8 => 256
Da jedoch das erste Bit auf das Vorzeichen entfällt, kann dieses nicht bzw. nur als Anzeiger ob Zahlen positiv oder negativ sind, verwendet werden. Somit halbiert sich der verfügbare Zeichenbereich für die positiven Zahlen. 256:2 => 128 => -128 - +127
Folglich fällt die Zahl +150 ausserhalb des möglichen Zahlenbereiches.

12. Bisher haben wir immer von ganzen Zahlen gesprochen. Oft genügt das in der realen
Welt aber nicht. Dazu ein Beispiel: Teile ich die Ganzzahl 1 durch die Ganzzahl 3 und
multipliziere sie darauf wieder mit der Ganzzahl 3 erhalte ich, sofern der
Compiler/Interpreter nicht trickst, die Ganzzahl 0, was bekanntlich falsch ist. Dies weil
das Resultat der Division nicht als 0.3333333 sondern als ganze Zahl 0
(Nachkommastellen werden ignoriert) zwischengespeichert wird. Benötigt wird also
ein Datentyp, der mit Fliesskommazahlen (Floating Point Numbers) klarkommt. Wie
würden sie eine solche Fliesskommazahl definieren, und wie sie digital abspeichern?
Machen sie dazu einen Vorschlag.

Die Verwendung der wissenschaftlichen Schreibweise für irrationale (R\Q=I) bzw. reele Zahlen (R) könnte hierbei Anhilfe schaffen.

y * 10^x = z.zzzzz
123456 * 10^2 = 12345600
123456 * 10^-2 = 1234.56

13. Erstellen sie die Wahrheitstabellen für die folgenden Funktionen:
a. Logisch UND/AND (mit zwei Eingangs- und einer Ausgangsvariablen)

| a | b | a & b |
-------- | -------- | -------- |
| w | w | w |
| w | f | f |
| f | w | f |
| f | f | f |

b. Logisch ODER/OR (mit zwei Eingangs- und einer Ausgangsvariablen)

| a | b | a & b |
-------- | -------- | -------- |
| w | w | w |
| w | f | w |
| f | w | w |
| f | f | f |

c. Logisch NICHT/NOT (mit einer Eingangs- und einer Ausgangsvariablen)

| a | Wahrheitswert |
-------- | -------- |
| !w | f |
| !f | w |

d. Logisch EXOR (mit zwei Eingangs- und einer Ausgangsvariablen)
-> Wird auch in der Kryptografie verwendet. z.B. Blockchiffren wie AES oder DES

| a | b | a & b |
-------- | -------- | -------- |
| w | w | f |
| w | f | w |
| f | w | w |
| f | f | f |

14. Eine in der Computertechnik wichtige mathematische Funktion ist die Restwert- oder
Modulo-Funktion mit dem in z.B. Java und C verwendeten Operationszeichen %.
Versuchen sie nun die folgende Berechnungen auszuführen. Was stellen sie fest?
a. 11 % 2 = 1
b. 10 % 2 = 0
c. 10 % 3 = 1
d. 10 % 5 = 0
e. 10 % 9 = 1


## A: DATEN CODIEREN 2: Alphanumerische Codes

1.	Welche der Dateien ist nun ASCII-codiert, welche UTF-8 und welche UTF-16 BE-BOM?

- Textsample1 -> ASCII
- Textsample2 -> UTF-8
- Textsample3 -> UTF-16 BE-ROM

2.	Alle drei Dateien enthalten denselben Text. Aus wie vielen Zeichen besteht dieser?

68 Zeichen

3.	Was sind die jeweiligen Dateigrössen? (Beachten sie, dass unter Grösse auf Datenträger jeweils 0 Bytes angegeben wird. Dies darum, weil beim Windows-Dateisystem NTFS kleine Dateien direkt in die MFT (Master File Table) geschrieben werden.) Wie erklären sie sich die Unterschiede?

- Textsample1 -> 68 Bytes
- Textsample2 -> 71 Bytes
- Textsample3 -> 138 Bytes

Durch die unterschiedlichen alphanumerischen Codes welche verwendet wurden (ASCII, UTF-8 und UTF-16 BE-ROM) und die dadurch unterschiedliche Menge an verwendeten Bits zur Darstellung bzw. Codierung der Zeichen unterscheidet sich die Grösse der Datei, nicht jedoch die Anzahl an dargestellten Zeichen.

4.	Bei den weiteren Fragen interessieren uns nur noch die ASCII- und die UTF-8-Datei: Bekanntlich ist UTF-8 in den ersten 128 Zeichen deckungsgleich mit ASCII. Untersuchen sie nun die beiden HEX-Dumps und geben sie an, welche Zeichen unterschiedlich codiert sind. Ein kleiner Tipp: Es sind deren zwei.

Der Umlaut "ä" sowie das Euro-Zeichen "€"

5.	Was bedeuten die beiden Ausdrücke, denen wir z.B. bei UTF-16 begegnen: Big-Endian (BE), Little-Endian (LE)?

Bei den beiden Ausdrücken Big-Endian (BE) und Little-Endian (LE) handelt es sich um zwei Möglichkeiten, wie die Bytes eines Mehrbyte-Wertes gespeichert werden können.

Bei Big-Endian wird der _höchstwertige_ Byte (MSB) an erster Stelle gespeichert, gefolgt vom _niedrigstwertigen_ Byte (LSB). Bei Little-Endian wird der LSB an erster Stelle gespeichert, gefolgt vom MSB.

Ein Beispiel: Das Unicode-Zeichen "A" hat den Wert 0x0041. In UTF-16BE wird dieses Zeichen als die Bytes 0x00 und 0x41 gespeichert. In UTF-16LE wird es als die Bytes 0x41 und 0x00 gespeichert.

Wenn ein Programm ein Unicode-Zeichen in UTF-16BE-Kodierung liest und davon ausgeht, dass es sich um UTF-16LE-Kodierung handelt, wird es das Zeichen falsch interpretieren. In diesem Fall würde das Programm das Zeichen "A" als "�" interpretieren.

6.	Im Notepad++ kann man unter dem Menüpunkt Codierung von ASCII zu UTF umschalten. Spielen sie damit etwas herum und notieren sie sich, was in der Darstellung jeweils ändert.

Zeichen welche nicht in der ASCII-Codierung, jedoch als UTF-Codierung vorhanden sind (z.B. Umlaute) werden bei Wahl von ASCII als Codierung nicht mehr korrekt dargestellt.

7.	Für Anspruchsvolle: Der UTF-8-Code kann je nach Zeichen ein, zwei, drei oder vier Byte lang sein. Wie kann der Textreader erkennen, wann ein UTF-8 Zeichencode beginnt und wann er endet? Untersuchen sie dies anhand der beiden Textsamples und lesen sie in z.B. Wikipedia die entsprechende Theorie zu UTF-8 durch. Tipp: Startbyte und Folgebyte.

-

## Reflexion

- Unterrichtsinhalte und Ziele:
Einführung in Codierungen wie ASCII oder UTF-8, Besprechung Möglichkeiten Nutzung von negativen Dezimalzahlen in binärer Darstellung

- Unterrichtsresultate:
Repetitiver Überblick über verschiedene Zeichencodierungen (ASCII, UTF-8, UTF-16) festigte bereits vorhandenes Wissen dazu. Ebenfalls sehr angenehme Erklärungen zu Unklarheiten einiger Aufgaben (Taktfrequenz, Seriell vs. Parallel etc.)

- Probleme/Knacknüsse:
Aufgaben zum Thema der negativen Dezimalzahlen in binärer Darstellung, waren eher komplex und erforderten Recherche und Nutzung von Tools (z.B. rapidtables.com).

- Neue Applikationen, Werkzeuge, Kommandos etc.:
hexed.it wurde im Unterricht verwendet, um den Inhalt einer Datei bzw. beispielsweise die den dargestellten ASCII-Zeichen zugeordneten Zahlenwerte in unterschiedlichen Zahlensystemen (BIN, DEC, HEX) darzustellen.

Unter GNU/Linux bzw. Bash kann der Befehl ''file'' sehr hilfreich sein um Informationen über die Codierung einer Datei zu erhalten.

Die Webseite rapidtables.com kann für die schnelle Umrechnung von BIN zu HEX oder die Überprüfung einer vorgängig manuell durchgeführten Berechnung sehr hilfreich sein.

- Offene Fragen:
Es bestehen zur Zeit keine offenen Fragen

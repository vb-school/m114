# Kapitel-D: KRYPTOGRAFIE
## Valentin Binotto


#### Aufgabe Rotationschiffre: 
Schon der römische Feldherr und spätere Kaiser Julius Cäsar kannte den folgenden Verschlüsselungstrick und nutzte ihn bei seinen geheimen Botschaften: Ersetze jeden Buchstaben durch den, der eine bestimmte Anzahl Stellen später im Alphabet folgt! Somit konnte Cäsar effektiv geheime Botschaften übermitteln, wie z.B. diese Zitate:

GHU DQJULII HUIROJW CXU WHHCHLW GLH ZXHUIHO VLQG JHIDOOHQ LFK NDP VDK XQG VLHJWH WHLOH XQG KHUUVFKH

DER ANGRIFF ERFOLGT ZUR TEEZEIT DIE WUERFEL SIND GEFALLEN ICH KAM SAH UND SIEGTE TEILE UND HERRSCHE

Benutzen Sie nun Ihr CrypTool1 und finden Sie heraus, um welche Zitate es sich handelt! Die Rotationschiffre ist übrigens ein klassisches, symmetrisches Verfahren. Nun aber nicht einfach drauflos probieren. Machen Sie etwas Kryptoanalyse mit einem ASCII-Histogramm. (Tipp: Häufigkeitsanalyse der im Text enthaltenen Buchstaben)

#### Aufgabe Vigenèreverschlüsselung: 

Um etwas warm zu laufen, verschlüsseln wird ohne Cryptool (!) das Wort BEEF mit dem Schlüsselwort AFFE.
=> BJJJ
Danach, wiederum ohne Cryptool, entschlüsseln wir den Geheimtext WRKXQT mit dem Schlüsselwort SECRET.
=> ENIGMA
Nun wirds wirklich spannend: Wir versuchen den Vigenère-Code zu knacken und bedienen uns einem Analysewerkzeug im Cryptool1. Heimlich abgehört haben wir die folgende Vigenère-Chiffre:

USP JHYRH ZZB GTV CJ WQK OCLGQVFQK GAYKGVFGX NS ISBVB MYBC MWCC NS JOEVB GTV KRQFV AGK XCUSP VFLVBLLBE ESSEILUBCLBXZU SENSWFGVRCES SER CZBCE ILUOLBPYISL CCSZG VZJ

DER STAAT BIN ICH ES IST AEUSSERST SCHWIERIG ZU REDEN OHNE VIEL ZU SAGEN ICH MACHE MIT JEDER ERNENNUNG NEUNUNDNEUNZIG UNZUFRIEDENE UND EINEN UNDANKBAREN LOUIS XIV

Neugierig wie wir sind, möchten wir gerne wissen, welcher Text hinter dieser Chiffre steckt. Da uns aber das Schlüsselwort fehlt, müssen wir tief in unsere Trickkiste greifen. (Tipp: Im CrypTool1/Hilfe/Index/Vigenère-Verschlüsselungsverfahren findet man weitere Informationen zum Vigenère-Analyseverfahren.)

Zu guter Letzt versuchen wir, ob das Analysetool auch Resultate liefert, wenn das Passwort wesentlich länger ist. Nehmen sie den entschlüsselten Text von vorhin und verschlüsseln sie ihn erneut, diesmal aber mit diesem Schlüssel:

LoremipsumdolorsitametconsectetueradipiscingelitAeneancommodoligulaegetdolorAeneanmassaCumsociisnatoquepenatibusetmagnisdisparturientmontesnasceturridiculusmusDonecquamfelisultriciesnecpellentesqueeupretiumquissemNullaconsequatmassaquisenimDonecpedejustofringillavelaliquetnecvulputateegetarcuInenimjustorhoncusutimperdietavenenatisvitaejustoNullamdictumfeliseupedemollispretiumIntegertinciduntCrasdapibusVivamuselementumsempernisiAeneanvulputateeleifendtellusAeneanleoligulaporttitoreuconsequatvitaeeleifendacenimAliquamloremantedapibusinviverraquisfeugiatatellusPhasellusviverranullautmetusvariuslaoreetQuisquerutrumAeneanimperdietEtiamultriciesnisivelaugueCurabiturullamcorperultriciesnisiNamegetduiEtiamrhoncusMaecenastempustellusegetcondimentumrhoncussemquamsemperliberositametadipiscingsemnequesedipsumNamquamnuncblanditvelluctuspulvinarhendreritidloremMaecenasnecodioetantetincidunttempusDonecvitaesapienutliberovenenatisfaucibusNullamquisanteEtiamsitametorciegeterosfaucibustinciduntDuisleoSedfringillamaurissitametnibhDonecsodalessagittismagnaSedconsequatleoegetbibendumsodalesauguevelitcursusnunc


=> JIY WBMEK TKU TWL WK MDZ ELYAEIIKV ZNBAAWVTM DB VMPIE GJUP PMWD DF YENIV UGY ECJSY QAL NPJIY IZZIEFWUR HIMFYYJRLYVLMX MPGFZVAWHPTI BRL QMEWP BYXEFCFLXIU PWGMJ PKC

Funktionieren nun die Vigenère-Analysetools immer noch?
=> Nein, stösst an seine Grenzen, da sehr langer Schlüssel

#### Aufgabe XOR-Stromchiffre: 
Verschlüsseln sie die Dezimalzahl 4711 von Hand als XOR-Stromchiffre. Der binäre Schlüssel lautet: 1000'1101. Zur Kontrolle entschlüsseln sie die erhaltene Chiffre wieder.
(Hinweis: Sie müssen die Dezimalzahl zuerst in eine 16-Bit Binärzahl umwandeln. Führende Nullen dabei nicht weglassen. Sollte der Schlüssel für die Verschlüsselung zu kurz sein, wird dieser mehrmals wiederholt. Der Datenstrom soll in dieser Aufgabe mit der Übertragung des MSB's, also von links nach rechts beginnen.)

```
4711 => 
TEXT 0001 0010 0110 0111
KEY  1000 1101 1000 1101
---------------------------
CHIF.1001 1111 1110 1010

=>
```



#### Weitere Aufgaben

⦁	Spielen sie in Cryptool1 einen Schlüsseltausch gemäss Diffie-Hellman durch. Experimentieren sie mit verschiedenen, auch eigenen Parametern. (Sie finden das Tool unter Einzelverfahren→Protokolle→Diffie-Hellman-Demo...)
    - -
⦁	RSA-Verschlüsselung: Erzeugen sie zwei asymmetrische Schlüsselpaare: Eines für «Muster Felix» und eines für «Hasler Harry» (Sie finden das Tool unter Digitale Signaturen/PKI→PKI→Schlüssel erzeugen/importieren...)
Verschlüsseln Sie nun eine Nachricht für Muster Felix und versuchen sie danach, den Text als Hasler Harry, danach als Muster Felix zu entschlüsseln. Was stellen sie fest? (Sie finden die Tools unter Ver-/Entschlüsseln→Asymmetrisch→RSA-Ver/Entschlüsselung...)
    - Verschlüsselung mit dem zum , für die Verschlüsselung verwendeten, Public Key gehörigen Private Key möglich. Heisst falls Nachricht mit Public Key von Muster Felix verschlüsselt wurde, ist entschlüsseln mit dem Private Key von Muster Felix möglich. Das Entschlüsseln des Chiffrats mit dem Private Key von Hasler Harry ist nicht möglich.

⦁	Im Gegensatz zum Diffie-Hellman-Verfahren (für Schlüsseltausch) kann RSA einen kompletten Text verschlüsseln. Sehen sie sich dazu  die RSA-Demo an. (Sie finden das Tool unter Ver-/Entschlüsseln→Asymmetrisch→RSA-Demo...)
    - -

⦁	Moderne Verschlüsselungsverfahren arbeiten hybrid. Schauen sie sich dazu die beiden Demos zu RSA-AES an. (Sie finden die Tools unter Ver-/Entschlüsseln→Hybrid→RSA-AES-Ver/Entschlüsselung...)
    - -

⦁	Wie kann ich den Public-Key verifizieren?
    - Mithilfe eines Fingerprints (Hash) kann ein Public-Key eindeutig indentifiziert werden. Der Fingerprint wird bestenfalls via einem weiteren Kanal dem Sender mitgeteilt: z.B. Persönliches Treffen
    - Alternativ kann via einem Web of Trust die Integrität des Public Keys verifiziert werden. (Ich vertraue X und weiss, dass Key XY X gehört, X vertraut A und weiss, dass A Key AY gehört) Das erfolgt über das gegenseitige SIgnieren von Public Keys.
    - Alternativ vertraue ich einer zentralen Stelle (z.B. CA bei TLS Zertifikaten) oder Root-Zertifikaten welche bereits durch den Hersteller auf der Computer-Hardware hinterlegt werden. Diese zentrale Stelle kümmert sich dann um das Verifizieren von Public Keys.

⦁	Was versteht man unter Public Key Infrastruktur?
    - Infrastruktur zum sicheren Verteilen von Public Keys. Die Integrität der Public Keys soll sichergestellt werden können. z.B. Schlüsselserver gehören zur PKI (http://pgp.mit.edu/)

⦁	Was bedeutet Certification-Authority (CA) und was Trust-Center (TC)?
    - Certification-Authority (CA):
    - Trust-Center (TC):

⦁	Finden sie heraus, wer das Zertifikat für die Bankwebseite www.ubs.com ausgestellt hat und wie lange es gültig ist.
    - DigiCert Inc. 12 Dezember 2024

⦁	Wiederholen sie das Ganze für die Schulwebseite www.tbz.ch
    - Let's Encrypt 29 Febraur 2024

⦁	Und zuletzt noch für die Webseite www.example.ch
    - Kein TLS Zertifikat vorhanden

⦁	Wählen sie irgendeine Applikation aus, die auf ihrem PC installiert ist. Stellen sie sich nun vor, sie müssten diese von Hand aktualisieren oder aus Kompatibilitätsgründen auf eine frühere Version zurückstufen. Wo finden sie aktuelle und frühere Versionen ihrer Software und wie wird sichergestellt, dass die dort angebotene SW-Version auch wirklich echt ist bzw. vom SW-Entwickler stammt?
    - Webseite des Entwicklers, mittels Zertifikaten Betriebssystemspezifisch (Entwicklerzertifikat für Windows, macOS etc.) oder/und mittels Web-of-Trust: Signierung mit asymetrischer Verschlüsselung

⦁	Erstellen sie eine virtuelle Linux-Maschine mit z.B. VirtualBox und Ubuntu. Richten sie nun auf ihrem WIN-PC eine Remoteverbindung via ssh zu ihrem Linux-PC ein. Überprüfen sie die Verbindung. Wäre auch eine graphische Anbindung möglich?
    - Theoretisch ja, Nachinstallieren von Packeten notwendig.

⦁	In dieser Übung untersuchen wir eine http-Verbindung und eine https-Verbindung mit dem Network-Sniffer Wireshark:
https://www.wireshark.org/
http://www.example.ch
https://www.zkb.ch
Untersuchen sie speziell die OSI-Layer 2,3,4 und 7. Was stellen sie fest? Wo liegen die Unterschiede zwischen http und https? Zusatzfrage: Kann man mit Wireshark bei einer https-Verbindung trotzdem herausfinden, welche Webseite besucht wurde?
    - Auf den OSI Layern 2, 3 und 4 unterscheiden sich die https nicht von den http Verbindungen. Die Verschlüsselung erfolgt erst zwischen Layer 4 und 5.

⦁	Öffnen sie die beiden folgenden Webseiten und achten sie auf die Unterschiede in der Webadresszeile. Was stellen sie bezüglich Protokoll und Zertifikat fest?
https://juergarnold.ch
https://www.zkb.ch
    - Unterschied der ausgestellten Zertikfikate. Gewisse TLS Zertifikate verlangen eine Überprüfung der Identität des Webseiteninhabers (z.B. mittels Handelsregisterauszug, Ausweiskopie etc.) und gewisse TLS Zertikfikate erfordern keine solchen Nachweise.

⦁	Wenn sie sich mit Zertifikaten befassen, fallen ihnen früher oder später folgende Anbieter bzw. Webseiten auf:
http://www.cacert.org
https://letsencrypt.org/de
Was genau wird hier zu welchen Konditionen angeboten?
    - Kostenlose Zertifikate für "everyone". Mission Internet zu verschlüsseln, kein Interesse an Profit. Wollen Web gegen simple Attacken auf unverschlüsselte Verbidnungen absichern.

⦁	Folgende TLS Zertifikatsarten werden unterschieden:
Domain Validated, Organization Validated und Extended Validation.
Sie möchten einen Webshop betreiben, wo mit Kreditkarte bezahlt werden kann. Welcher Zertifikatstyp ist der richtige?
    - Soweit mein Unternehmen keine Reputation aufzuweisen hat, lässt sich mit dem "Domain Validated" Zertifikat arbeiten, da dieses eine verschlüsselte Übertragung der Kreditkartendaten sicherstellt. Sobald mein Unternehmen jedoch eine gewisse Reputation besitzt, also den Kunden bereits durch den Firmennamen bekannt ist, würde ich mindestens auf ein "Organization Validated" Zertikfikat zurückgreifen, da damit auch mein Firmenname verifiziert wird und der Kunde davon ausgehen darf, dass dieser Webshop tatsächlich von meinem Unternehmen stammt.

⦁	Studieren sie den Beitrag auf der Webseite Let's Encrypt "Wie es funktioniert"
https://letsencrypt.org/de/how-it-works/
Was ist der Unterschied zwischen OpenPGP und X.509?
- X.509 ist ein ITU-T-Standard für eine PKI (Public-Key-Infrastruktur) zum Erstellen digitaler Zertifikate und basiert auf einer strikten Hierarchie, da die Zertifikate von CAs ausgestellt und signiert werden. Alternative zum Web-of-Trust
- OpenPGP funktioniert voll und ganz nach dem Web-of-Trust Prinzip. Vertraut X mir und ich vertraue Z, vertraut X auch Z.

⦁	Erklären sie den Aufruf einer sicheren Webseite. (HTTPS)
Wie ist der Ablauf beim Protokoll TLS? Wo genau kommen die Zertifikate ins Spiel?
- a. Webseitenaufruf:
    - Du gibst eine HTTPS-URL in deinen Browser ein.

- b. Zertifikatsaustausch:
    - Die Webseite schickt dir ein digitales Zertifikat, das von einer vertrauenswürdigen Quelle signiert ist.

- c. Zertifikatsprüfung:
    - Dein Browser überprüft das Zertifikat, um sicherzustellen, dass es echt ist.

- d. Schlüsselaustausch:
    - Dein Browser und die Webseite tauschen verschlüsselte Schlüssel aus, um eine sichere Verbindung herzustellen.

- e. Verschlüsselte Kommunikation:
    - Die eigentliche Datenübertragung erfolgt jetzt verschlüsselt, sodass Dritte nicht mitlesen können.

Durch diesen Prozess stellt HTTPS sicher, dass deine Daten sicher über das Internet übertragen werden. Zertifikate spielen eine Rolle, um die Identität der Webseite zu bestätigen und die Verschlüsselung einzurichten.

⦁	Was bedeutet S/MIME?
- S/MIME steht für "Secure/Multipurpose Internet Mail Extensions" und ist ein Verschlüsselungs- und Signaturstandard für E-Mails. Es ermöglicht die sichere Verschlüsselung von E-Mail-Inhalten, um die Vertraulichkeit und Integrität der Nachrichten zu gewährleisten. Außerdem erlaubt S/MIME die digitale Signatur von E-Mails, um die Authentizität des Absenders zu überprüfen und die Nachrichten vor Fälschungen zu schützen.

⦁	Aus gesetzlichen Gründen sind sie verpflichtet, den gesamten geschäftlichen EMail-Verkehr zu archivieren, auch den verschlüsselten. Was ist das Problem dabei und wie könnte man dies lösen?
- Problem: Verschlüsselte E-Mails schützen die Daten, aber erschweren das längerfristige Archivieren, da nur der Besitzer des privaten Schlüssels sie entschlüsseln kann.

- Lösung: Private Schlüssel zentral auf einem abgesicherten Server speichern. Emails können so bei Bedarf entschlüsselt werden. Weitere rechtliche Schranken und Bestimmungen sind zu beachten.



### Reflexion

- Unterrichtsinhalte und Ziele:
Repetition bereits mir persönlich bekannter Themen (symmetrische und asymmetrische Verschlüsselung, AES, PGP, Public-Key-Infrastruktur (CAs, Web-of-Trust)

- Unterrichtsresultate:
Keine grossen Wissensgewinne -> Bestehendes Wissen/Themen wurden repetiert. Insbesondere Wissensgewinn bei Analyse von HTTPS Verbindung mittels Wireshark.

- Probleme/Knacknüsse:
Keine grösseren Herausfoirderungen, da grosse Teile der Unterrichtsinhalte im Be3reich Kryptografie mir bereits bekannt.

- Neue Applikationen, Werkzeuge, Kommandos etc.:
Mozilla Thunderbird -> Bereits installiert
Wireshark -> Bereits installiert
Zertifikatsanalysetools innerhalb des Browsers (Firefox) -> Bereits verwendet

- Offene Fragen:
Es bestehen zur Zeit keine offenen Fragen





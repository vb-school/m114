# Modul 114 - Valentin Binotto
## Kapitel-B: DATEN KOMPRIMIEREN

### 1.	Huffman-Algorithmus: (Teamarbeit). 
Jeder denkt für sich ein Wort mit ca. 15 Buchstaben aus und erstellt dazu die Huffman-Codetabelle und das entsprechend komprimierte Wort in HEX-Darstellung. Nun werden die Codes inklusive der Codetabelle gegenseitig ausgetauscht. Kann ihr Partner ihr gewähltes Wort richtig dekomprimieren?

TELEKOMMUNIKATION

E = 2
T = 2
L = 1
K = 2
O = 2
M = 2
U = 1
N = 2
I = 2
A = 1

T E K O M N I A U L
1 2 3 4 5 6 7 8 9 10

1^0
```
          17
         / 15
        / / 13
       / / / 11
      / / / / 9
     / / / / / 7
    / / / / / / 5
   / / / / / / / 3
  / / / / / / / /2
 / / / / / / / / ^
T E K O M N I A U L
2 2 2 2 2 2 2 1 1 1
```

```
T = 1
E = 01
K = 001
O = 0001
M = 00001
N = 000001
I = 0000001
A = 00000001
U = 000000001
L = 000000000
```

### 2.	RLC/E-Verfahren: 
Sie erhalten diesen RL-Code:
01010001 11100100 10010010 01001001 00100101 10010110 01001001 00100100 10010010 001
Folgendes ist ihnen dazu bekannt: Es handelt sich um eine quadratische Schwarz-Weiss-Rastergrafik mit einer Kantenlänge von 8 Pixel. Es wird links oben mit der Farbe Weiss begonnen. Eine Farbe kann sich nicht mehr als siebenmal wiederholen. Zeichnen sie die Grafik auf. Was stellt sie dar?
```
010 2e
100 4e
011 3e
110 6e
010 2e
010 2e
010 2e
010 2e
010 2e
010 2e
010 2e
010 2e
010 2e
110 6e
010 2e
110 6e
010 2e
010 2e
010 2e
010 2e
010 2e
010 2e
010 2e
010 2e
001 1e

XXOOOOXX
XOOOOOOX
XOOXXOOX
XOOXXOOX
XOOOOOOX 
XOOOOOOX
XOOXXOOX
XOOXXOOX
```

### 3. LZW-Verfahren
Erstellen sie die LZW-Codierung für das Wort «ANANAS» und überprüfen sie mit der Dekodierung ihr Resultat. Danach versuchen sie den erhaltenen LZW-Code «ERDBE<256>KL<260>» zu dekomprimieren.

```
ANANAS

A -> AN -> 256
N -> NA -> 257
A -> AN -> AN -> 256 -> ANA -> 258
A -> AS -> 259

AN256AS
```

ERDBE256KL260

R -> ER -> 256
D -> RD -> 257
B -> DB -> 258
E -> BE -> 259
256 -> ER -> EE -> 260
K -> ERK -> 261
L -> KL -> 262

ERDBE256KL260
ERDBEERKLEE



### 4.	ZIP-Komprimierung
Wir wollen die Effizienz bei der ZIP-Komprimierung untersuchen. Dazu sollen sie ASCII-Textdateien erstellen.
a.	Die erste enthält 10, die zweite 100, die dritte 1000, die vierte 10'000 und die fünfte 100'000 ASCII-Zeichen
b.	Achten sie darauf, dass die Zeichen möglichst zufällig gewählt werden. Auf dem Internet findet man entsprechende Textgeneratoren.
c.	Kopieren sie jede dieser fünf Textdateien in eine eigene ZIP-Datei. In der Folge erhalten sie fünf ZIP-Dateien.
d.	Werten sie nun in einer EXCEL-Tabelle die erforderlichen Speichergrössen aus: ASCII-Datei-Grösse zu ZIP-Datei-Grösse. Versuchen sie nun, ihr Resultat zu interpretieren bzw. zu begründen. Tipp: Sie können in EXCEL Zahlenreihen auch grafisch anzeigen.

10Zeichen : 128
100Zeichen : 135
1000Zeichen : 143
10000Zeichen : 172
100000Zeichen : 348

e.	Nun legen wir noch einen drauf: Erstellen sie eine ASCII-Textdatei mit 100'000 Zeichen. Diesmal aber nicht zufällig (random) befüllt, sondern ausschliesslich mit dem Buchstaben A, danach zippen sie. Vergleichen sie nun die beiden ZIP-Dateien. Wie erklären sie sich den Unterschied der Speichergrössen?
f.	Zu guter Letzt wollen wir untersuchen, was die ZIP-Komprimierung bringt, wenn die Originaldatei, wie beim JPG-Bildformat, bereits komprimiert (DCT) vorliegt. Dazu erhalten sie die zwei folgenden Bilder:
https://www.juergarnold.ch/Kompression/ZIPTestHi.jpg
https://www.juergarnold.ch/Kompression/ZIPTestLo.jpg
.Gehen sie nun gleich vor, wie beim vorangegangenen Untersuch der Textdateien. Begründen sie ihr Resultat.


### 5.	BWT (Burrows-Wheeler-Transformation):
a.	Erstellen sie die BWT-Transformation für das Wort ANANAS und überprüfen sie mit der Rücktransformation ihr Resultat.
```
ANANAS
SANANA
ASANAN
NASANA
ANASAN
NANASA

ANANAS
ANASAN
ASANAN
NANASA
NASANA
SANANA

SNNAAA - 1
123456

123456
AAANNS - 1
456231

ANANAS
```
b.	Sie erhalten den Code IICRTGH6 in der Burrows-Wheeler-Transformation. Welches Wort verbirgt sich dahinter?
```
IICRTGH - 6
1234567

1234567
CGHIIRT
3671245

RICHTIG
```

•	Unterrichtsziele:
Einführung in die Komprimierung von Daten, Methoden zur Datenkomprimierung kennenlernen
•	Unterrichtsresultate:
Verschiedene Algorithmen zur Komprimierung von Daten oder zur Vorbereitung vor der Komprimierung (BWT) betrachtet.
•	Probleme/Knacknüsse:
Aufgaben zu den ZIP-Dateien eher aufwändig
•	Neu Tools:
Alle genutzten Tools bereits bekannt
•	Offene Fragen:
-
